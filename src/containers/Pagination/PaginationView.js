import React, { Component } from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

class PaginationView extends Component {
  render() {
    const { pages, page, last, onClick } = this.props;

    if (!pages.length)
      return null;

    return (
      <Pagination>
        <PaginationItem disabled={page <= 1}>
          <PaginationLink onClick={e => onClick(page - 1)} previous tag="button"></PaginationLink>
        </PaginationItem>

        { pages.map((item, key) => {
          return (
            <PaginationItem key={key} disabled={!Number.isInteger(item) || page === item} active={page === item}>
              <PaginationLink onClick={e => onClick(item)} tag="button">{ item }</PaginationLink>
            </PaginationItem>
          )
        }) }

        <PaginationItem disabled={page >= last}>
          <PaginationLink onClick={e => onClick(page + 1)} next tag="button"></PaginationLink>
        </PaginationItem>
      </Pagination>
    );
  }
}

export default PaginationView;
