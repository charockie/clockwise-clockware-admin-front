import React, { Component } from 'react';
import PaginationView from "./PaginationView";

class PaginationContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {onEachSide: 1};
  }

  getPages() {
    let onEachSide = this.state.onEachSide,
      middlePages = onEachSide * 3,
      pages = [],
      page = this.props.page,
      last = this.props.last;

    if (last <= 1)
      return pages;

    if (page <= middlePages)
      return this.fromStartPages(middlePages);

    if (page > (last - middlePages))
      return this.toEndPages(middlePages);

    return this.fullPages(onEachSide);
  }

  fromStartPages(middlePages) {
    let pages = [],
      last = this.props.last;

    for (let i = 1; i <= middlePages + 2; i++) {
      if (i <= last)
        pages.push(i);
    }

    if (pages.pop() < last) {
      pages.push("...");
      pages.push(last);
    } else
      pages.push(last);

    return pages;
  }

  toEndPages(middlePages) {
    let pages = [],
      last = this.props.last;

    pages.push(1);
    pages.push("...");

    let from = last - (middlePages);
    for (let i = from; i <= last; i++) {
      pages.push(i);
    }

    return pages;
  }

  fullPages(onEachSide) {
    let pages = [],
      page = this.props.page,
      last = this.props.last;

    pages.push(1);
    pages.push("...");

    for (let i = (page - onEachSide); i <= (page + onEachSide); i++) {
      pages.push(i);
    }

    pages.push("...");
    pages.push(last);

    return pages;
  }

  render() {
    return (
      <PaginationView {...this.props} pages={this.getPages()} />
    );
  }
}

export default PaginationContainer;
