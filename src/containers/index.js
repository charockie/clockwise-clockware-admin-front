import DefaultLayout from './DefaultLayout';
import Pagination from './Pagination';

export { DefaultLayout, Pagination };
