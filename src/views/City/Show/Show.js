import React, { Component } from 'react';
import {
  Button, ButtonGroup,
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
} from 'reactstrap';
import { Link } from 'react-router-dom';

class Show extends Component {
  render() {
    const { id, name } = this.props.city;

    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                Просмотр города №{id}

                <ButtonGroup className="pull-right">
                  <Link to={`/cities/${id}/edit`} className="btn btn-primary" title="Редактирова"><i className="fa fa-edit"></i></Link>
                  <Button onClick={e => this.props.onDelete(id)} color="danger" title="Удалить"><i className="fa fa-trash"></i></Button>
                </ButtonGroup>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" md="6" xl="6">
                    <p>Название города: {name}</p>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Show;
