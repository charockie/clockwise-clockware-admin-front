import React, { Component } from 'react';
import Show from './Show';
import { connect } from 'react-redux';
import { fetchCity, deleteCity } from '../../../store/City/actions';
import { createLoadingSelector } from '../../../store/api/selectors';
import Loading from '../../../containers/Loading';

class ShowContainer extends Component {
  constructor(props) {
    super(props);

    this.onDelete = this.onDelete.bind(this);
  }

  componentWillMount() {
    const cityId = this.props.match.params.id;
    this.props.fetchCity(cityId);
  }

  onDelete(id) {
    this.props.deleteCity(
      id,
      () => this.props.history.push(`/cities`)
    );
  }

  render() {
    if (this.props.isFetching || !this.props.city)
      return <Loading/>;

    return (
      <Show
        city={this.props.city}
        onDelete={this.onDelete}
      />
    );
  }
}


const loadingSelector = createLoadingSelector(['GET_CITY']);
const mapStateToProps = state => {
  return {
    city: state.city.show,
    isLoading: loadingSelector(state),
  };
}

const mapDispatchToProps = {
  fetchCity,
  deleteCity,
}

export default connect(mapStateToProps, mapDispatchToProps)(ShowContainer);

