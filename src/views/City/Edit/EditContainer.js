import React, { Component } from 'react';
import { editCity, updateCity } from '../../../store/City/actions';
import { connect } from 'react-redux';
import Edit from './Edit';
import Loading from '../../../containers/Loading';

class EditContainer extends Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillMount() {
    const cityId = this.props.match.params.id;
    this.props.editCity(cityId);
  }

  onSubmit(event) {
    event.preventDefault();

    return this.props.updateCity(
      this.props.form.id,
      this.props.form,
      (id) => this.props.history.push(`/cities/${id}`)
    );
  }

  render() {
    if (!this.props.form || !this.props.form.id)
      return <Loading/>;

    return (
      <Edit
        id={this.props.form.id}
        name={this.props.form.name}
        onSubmit={this.onSubmit}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    form: state.city.form,
  };
}

const mapDispatchToProps = {
  editCity,
  updateCity,
}

export default connect(mapStateToProps, mapDispatchToProps)(EditContainer);
