import React, { Component } from 'react';
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Col,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
  ButtonGroup,
  FormFeedback,
} from 'reactstrap';
import PropTypes from 'prop-types';

class FormView extends Component {
  render() {
    const { onSubmit, onReset, onChange, isLoading, hasError, getError, id, name, email, city_id, citiesList, } = this.props;

    return (
      <div className="animated fadeIn">

        <Card>
          <CardHeader>
            <h3>
              <strong>Редактирование</strong> клиента №{ id }
            </h3>
          </CardHeader>
          <CardBody>
            <Form onSubmit={onSubmit} className="form-horizontal">
              <FormGroup row>
                <Col md="3">
                  <Label htmlFor="name">Имя</Label>
                </Col>
                <Col xs="12" md="9">
                  <Input type="text"
                         id="name"
                         name="name"
                         value={name}
                         onChange={onChange}
                         placeholder="Введите имя..."
                         invalid={ hasError("name") }
                  />
                  <FormFeedback>{ getError("name") }</FormFeedback>
                </Col>
              </FormGroup>

              <FormGroup row>
                <Col md="3">
                  <Label htmlFor="email">Email</Label>
                </Col>
                <Col xs="12" md="9">
                  <Input type="email"
                         id="email"
                         name="email"
                         value={email}
                         onChange={onChange}
                         placeholder="Введите email..."
                         invalid={ hasError("email") }
                  />
                  <FormFeedback>{ getError("email") }</FormFeedback>
                </Col>
              </FormGroup>

              <FormGroup row>
                <Col md="3">
                  <Label htmlFor="city_id">Город</Label>
                </Col>
                <Col xs="12" md="9">
                  <Input type="select"
                         id="city_id"
                         name="city_id"
                         value={city_id}
                         onChange={onChange}
                         invalid={ hasError("city_id") }
                  >
                    <option disabled value={''}>Выберите город</option>
                    { Object.keys(citiesList).map((id) => {
                      return (<option value={id} key={id}>{citiesList[id]}</option>);
                    })}
                  </Input>
                  <FormFeedback>{ getError("city_id") }</FormFeedback>
                </Col>
              </FormGroup>

            </Form>
          </CardBody>
          <CardFooter className="text-right">
            <ButtonGroup size="sm">
              <Button type="reset" onClick={onReset} color="danger" disabled={isLoading}><i className="fa fa-ban"></i> Сброс</Button>
              <Button type="submit" onClick={onSubmit} color="success" disabled={isLoading}><i className="fa fa-dot-circle-o"></i> Сохранить</Button>
            </ButtonGroup>
          </CardFooter>
        </Card>

      </div>
    );
  }
}

FormView.propTypes = {
  id: PropTypes.number,
  name: PropTypes.string,
  email: PropTypes.string,
  city_id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),

  isLoading: PropTypes.bool.isRequired,

  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onReset: PropTypes.func.isRequired,
  hasError: PropTypes.func.isRequired,
  getError: PropTypes.func.isRequired,
};

export default FormView;
