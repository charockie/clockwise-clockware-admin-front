import React, { Component } from 'react';
import { connect } from 'react-redux';
import Login from './Login';
import { authenticate, changeForm } from '../../store/Login/actions'
import { createLoadingSelector, createValidationErrorsListSelector } from '../../store/api/selectors';
import { resetValidationErrors } from '../../store/api/actions';

class LoginContainer extends Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
    this.getError = this.getError.bind(this);
    this.hasError = this.hasError.bind(this);
  }

  componentWillUnmount() {
    this.props.resetValidationErrors(["LOGIN"]);
  }

  onSubmit(event) {
    event.preventDefault();

    this.props.authenticate(this.props.form);
  }

  onChange = (event) => {
    this.props.changeForm(event.target.name, event.target.value);
  }

  hasError() {
    return this.props.errors && Object.keys(this.props.errors).length !== 0;
  }

  getError() {
    if (!this.hasError())
      return;

    let errors = this.props.errors;
    return errors[Object.keys(errors)[0]][0];
  }

  render() {
    return (
      <Login
        email={this.props.form.email}
        password={this.props.form.password}
        isLoading={this.props.isLoading}
        errors={this.props.errors}

        getError={this.getError}
        onChange={this.onChange}
        onSubmit={this.onSubmit}
      />
    );
  }
}

const loadingSelector = createLoadingSelector(['LOGIN']);
const validationErrorsListSelector = createValidationErrorsListSelector(['LOGIN']);
const mapStateToProps = state => {
  return {
    form: state.login.form,
    isLoading: loadingSelector(state),
    errors: validationErrorsListSelector(state),
    isAuthorised: state.login.isAuthenticated
  };
}

const mapDispatchToProps = {
  changeForm,
  authenticate,
  resetValidationErrors,
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
