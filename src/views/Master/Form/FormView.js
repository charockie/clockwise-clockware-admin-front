import React, { Component } from 'react';
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Col,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
  ButtonGroup,
  FormFeedback,
  FormText,
  CardImg
} from 'reactstrap';
import PropTypes from 'prop-types';

class FormView extends Component {
  render() {
    const { onSubmit, onReset, onChange, onChangeCities, onFileChange, onFileDelete, isLoading, hasError, getError, id, name, surname, rating, rank_id, cities, image_id, image_url, ranksList, citiesList, } = this.props;

    return (
      <div className="animated fadeIn">

        <Card>
          <CardHeader>
            {id ? (
              <h3>
                <strong>Редактирование</strong> мастера №{ id }
              </h3>
            ) : (
              <h3>
                <strong>Добавление</strong> нового мастера
              </h3>
              )
            }
          </CardHeader>
          <CardBody>
            <Form onSubmit={onSubmit} className="form-horizontal">
              <FormGroup row>
                <Col md="3">
                  <Label htmlFor="name">Имя</Label>
                </Col>
                <Col xs="12" md="9">
                  <Input type="text"
                         id="name"
                         name="name"
                         value={name}
                         onChange={onChange}
                         placeholder="Введите имя..."
                         invalid={ hasError("name") }
                  />
                  <FormFeedback>{ getError("name") }</FormFeedback>
                </Col>
              </FormGroup>

              <FormGroup row>
                <Col md="3">
                  <Label htmlFor="surname">Фамилия</Label>
                </Col>
                <Col xs="12" md="9">
                  <Input type="text"
                         id="surname"
                         name="surname"
                         value={surname}
                         onChange={onChange}
                         placeholder="Введите фамилию..."
                         invalid={ hasError("surname") }
                  />
                  <FormFeedback>{ getError("surname") }</FormFeedback>
                </Col>
              </FormGroup>

              <FormGroup row>
                <Col md="3">
                  <Label htmlFor="rating">Рейтинг</Label>
                </Col>
                <Col xs="12" md="9">
                  <Input type="number"
                         step={0.1}
                         id="rating"
                         name="rating"
                         value={rating}
                         onChange={onChange}
                         placeholder="Укажите рейтинг..."
                         invalid={ hasError("rating") }
                  />
                  <FormFeedback>{ getError("rating") }</FormFeedback>
                </Col>
              </FormGroup>

              <FormGroup row>
                <Col md="3">
                  <Label htmlFor="rank_id">Ранг</Label>
                </Col>
                <Col xs="12" md="9">
                  <Input type="select"
                         id="rank_id"
                         name="rank_id"
                         value={rank_id}
                         onChange={onChange}
                         invalid={ hasError("rank_id") }
                  >
                    <option value={''}>Выберите ранг...</option>
                    { Object.keys(ranksList).map((id) => {
                      return (<option value={id} key={id}>{ranksList[id]}</option>);
                    })}
                  </Input>
                  <FormFeedback>{ getError("rank_id") }</FormFeedback>
                </Col>
              </FormGroup>

              <FormGroup row>
                <Col md="3">
                  <Label htmlFor="cities">Города</Label>
                </Col>
                <Col xs="12" md="9">
                  <Input type="select"
                         multiple
                         id="cities"
                         name="cities"
                         value={cities}
                         onChange={onChangeCities}
                         invalid={ hasError("cities") }
                  >
                    <option disabled value={''}>Выберите города...</option>
                    { Object.keys(citiesList).map((id) => {
                      return (<option value={id} key={id}>{citiesList[id]}</option>);
                    })}
                  </Input>
                  <FormFeedback>{ getError("cities") }</FormFeedback>
                </Col>
              </FormGroup>

              <FormGroup row>
                <Col md="3">
                  <Label htmlFor="rating">Фото</Label>
                </Col>
                <Col xs="12" md="9">
                  { !image_url ? (
                    <Col>
                      <Input type="file"
                             onChange={onFileChange}
                             invalid={ hasError("file") }
                      />
                      <FormText color="muted">
                        Фото должно иметь соотношение сторон 3:2 и ширину не менее 255 пикселей
                      </FormText>
                      <FormFeedback>{ getError("file") }</FormFeedback>
                    </Col>
                  ) : (
                    <Col xs="12" md="8">
                      <CardImg src={image_url} />
                      <Button className="btn btn-danger btn-sm btn-block"
                              onClick={onFileDelete}
                              title="Удалить картинку"
                      ><i className="fa fa-trash"></i></Button>
                    </Col>
                    )}
                </Col>
              </FormGroup>

            </Form>
          </CardBody>
          <CardFooter className="text-right">
            <ButtonGroup size="sm">
              <Button type="reset" onClick={onReset} color="danger" disabled={isLoading}><i className="fa fa-ban"></i> Сброс</Button>
              <Button type="submit" onClick={onSubmit} color="success" disabled={isLoading}><i className="fa fa-dot-circle-o"></i> Сохранить</Button>
            </ButtonGroup>
          </CardFooter>
        </Card>

      </div>
    );
  }
}

FormView.propTypes = {
  id: PropTypes.number,
  name: PropTypes.string,
  surname: PropTypes.string,
  rating: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  rank_id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  image_id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  image_url: PropTypes.string,

  isLoading: PropTypes.bool.isRequired,

  onChange: PropTypes.func.isRequired,
  onFileChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onReset: PropTypes.func.isRequired,
  hasError: PropTypes.func.isRequired,
  getError: PropTypes.func.isRequired,
};

export default FormView;
