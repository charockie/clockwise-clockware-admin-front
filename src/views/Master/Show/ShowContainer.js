import React, { Component } from 'react';
import Show from './Show';
import { connect } from 'react-redux';
import { fetchMaster, deleteMaster } from '../../../store/Master/actions';
import { createLoadingSelector } from '../../../store/api/selectors';
import Loading from '../../../containers/Loading';

class ShowContainer extends Component {
  constructor(props) {
    super(props);

    this.onDelete = this.onDelete.bind(this);
  }

  componentWillMount() {
    const masterId = this.props.match.params.id;
    this.props.fetchMaster(masterId);
  }

  onDelete(id) {
    this.props.deleteMaster(
      id,
      () => this.props.history.push(`/masters`)
    );
  }

  render() {
    if (this.props.isFetching || !this.props.master)
      return <Loading/>;

    return (
      <Show
        master={this.props.master}
        onDelete={this.onDelete}
      />
    );
  }
}


const loadingSelector = createLoadingSelector(['GET_MASTER']);
const mapStateToProps = state => {
  return {
    master: state.master.show,
    isLoading: loadingSelector(state),
  };
}

const mapDispatchToProps = {
  fetchMaster,
  deleteMaster,
}

export default connect(mapStateToProps, mapDispatchToProps)(ShowContainer);

