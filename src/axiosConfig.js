import axios from 'axios';

axios.defaults.baseURL = process.env['REACT_APP_API_URL'];
axios.interceptors.request.use(
  config => {
    if (localStorage.hasOwnProperty('api_key')) {
      config.headers.Authorization = `Bearer ${localStorage.getItem('api_key')}`;
    }

    return config;
  },
  error => {
    console.log("AXIOS error", error);
    Promise.reject(error)
  }
);
