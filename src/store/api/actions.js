export const RESET_VALIDATION_ERRORS = 'RESET_VALIDATION_ERRORS';

export const resetValidationErrors = (keys = []) => ({
  type: RESET_VALIDATION_ERRORS,
  payload: keys,
})
