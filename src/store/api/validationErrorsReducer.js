export const validationErrorsReducer = (state = {}, action) => {
  const { type, payload } = action;

  if (type === "RESET_VALIDATION_ERRORS")
    return resetValidationErrorsReducer(state, action);

  const matches = /(.*)_(REQUEST|FAILURE)/.exec(type);

  if (!payload || payload.status !== 422) return  state;

  // not a *_REQUEST / *_FAILURE actions, so we ignore them
  if (!matches) return state;

  const [, requestName, requestState] = matches;
  return {...state,
    // Store errorMessage
    [requestName]: requestState === 'FAILURE' ? payload.data.errors : '',
  };
};

const resetValidationErrorsReducer = (state = {}, action) => {
  return {};
};
