import {
  GET_RANKS_LIST_SUCCESS
} from './actions';

const defaultState = {
  list: {},
}

export const rankReducer = (state = defaultState, action) => {
  switch (action.type) {
    case GET_RANKS_LIST_SUCCESS:
      return { ...state,
        list: action.payload
      };

    default:
      return state;
  }
}
