import {
  GET_CITIES_LIST_SUCCESS,
  GET_CITIES_SUCCESS,
  GET_CITY_SUCCESS,
  STORE_CITY_SUCCESS,
  EDIT_CITY_SUCCESS,
  UPDATE_CITY_SUCCESS,
  DELETE_CITY_SUCCESS,

  CHANGE_PAGE,
  CHANGE_FORM,
  RESET_FORM,
} from './actions';

const defaultState = {
  list: {
    data: [],
    links: {},
    meta: {
      current_page: 1,
      from: null,
      last_page: 1,
      path: "",
      per_page: 10,
      to: null,
      total: 0,
    },
  },
  shortList: {},
  show: null,
  form: {
    id: null,
    name: '',
  },
}

export const cityReducer = (state = defaultState, action) => {
  switch (action.type) {
    case GET_CITIES_LIST_SUCCESS:
      return { ...state,
        shortList: action.payload
      };

    case GET_CITIES_SUCCESS:
      return { ...state,
        list: action.payload
      };

    case GET_CITY_SUCCESS:
      return { ...state,
        show: action.payload
      };

    case CHANGE_PAGE:
      return setPage(state, action.payload);

    case CHANGE_FORM:
      const {name, value} = action.payload;
      return { ...state,
        form: {...state.form, [name]: value}
      };

    case RESET_FORM:
      return { ...state,
        form: {...defaultState.form,
          id: action.payload ? null : state.form.id
      }
      };

    case STORE_CITY_SUCCESS:
      state.list.data.push(action.payload);
      return state;

    case EDIT_CITY_SUCCESS:
      return { ...state,
        form: { ...state.form,
          id: action.payload.id,
          name: action.payload.name,
        }
      };

    case UPDATE_CITY_SUCCESS:
      return state;

    case DELETE_CITY_SUCCESS:
      return { ...state,
        list: { ...state.list,
          data: [
            ...state.list.data.filter(item => item.id !== action.payload)
          ],
          meta: { ...state.list.meta,
            total: --state.list.meta.total
          }
        }
      };

    default:
      return state;
  }
}

function setPage(state, newPage) {
  return { ...state,
    list: { ...state.list,
      meta: { ...state.list.meta,
        current_page: newPage
      }
    }
  }
}
