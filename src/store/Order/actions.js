import axios from 'axios';

export const GET_ORDERS_REQUEST = 'GET_ORDERS_REQUEST';
export const GET_ORDERS_SUCCESS = 'GET_ORDERS_SUCCESS';
export const GET_ORDERS_FAILURE = 'GET_ORDERS_FAILURE';

export const GET_ORDER_REQUEST = 'GET_ORDER_REQUEST';
export const GET_ORDER_SUCCESS = 'GET_ORDER_SUCCESS';
export const GET_ORDER_FAILURE = 'GET_ORDER_FAILURE';

export const EDIT_ORDER_REQUEST = 'EDIT_ORDER_REQUEST';
export const EDIT_ORDER_SUCCESS = 'EDIT_ORDER_SUCCESS';
export const EDIT_ORDER_FAILURE = 'EDIT_ORDER_FAILURE';

export const UPDATE_ORDER_REQUEST = 'UPDATE_ORDER_REQUEST';
export const UPDATE_ORDER_SUCCESS = 'UPDATE_ORDER_SUCCESS';
export const UPDATE_ORDER_FAILURE = 'UPDATE_ORDER_FAILURE';

export const DELETE_ORDER_REQUEST = 'DELETE_ORDER_REQUEST';
export const DELETE_ORDER_SUCCESS = 'DELETE_ORDER_SUCCESS';
export const DELETE_ORDER_FAILURE = 'DELETE_ORDER_FAILURE';

export const CHANGE_PAGE = 'CHANGE_PAGE';

export const CHANGE_FORM = 'CHANGE_FORM';
export const RESET_FORM = 'RESET_FORM';

export const changeForm = (name, value) => ({
  type: CHANGE_FORM,
  payload: {name: name, value: value},
})

export function resetForm(resetId = false) {
  return (dispatch) => {
    dispatch({ type: RESET_FORM, payload: resetId });
    dispatch({ type: "RESET_VALIDATION_ERRORS" });
  };
}

export function changePage(newPage) {
  return (dispatch) => {
    this.fetchOrdersList(newPage);

    dispatch({
      type: CHANGE_PAGE,
      payload: newPage
    });
  }
}

export function fetchOrdersList(current_page = 1, per_page = 10) {
  return (dispatch) => {
    dispatch({ type: GET_ORDERS_REQUEST });
    return axios.get(`/api/admin/orders?page=${current_page}&per_page=${per_page}`)
      .then(response => dispatch({ type: GET_ORDERS_SUCCESS, payload: response.data }) )
      .catch(error => dispatch({ type: GET_ORDERS_FAILURE, payload: error, error: true }) );
  }
}

export function fetchOrder(id) {
  return (dispatch) => {
    dispatch({ type: GET_ORDER_REQUEST });
    return axios.get(`api/admin/orders/${id}`)
      .then(response => dispatch({ type: GET_ORDER_SUCCESS, payload: response.data.data }) )
      .catch(error => dispatch({ type: GET_ORDER_FAILURE, payload: error, error: true }) );
  }
}

export function editOrder(id, afterSuccess) {
  return (dispatch) => {
    dispatch({ type: EDIT_ORDER_REQUEST });
    return axios.get(`api/admin/orders/${id}`)
      .then(response => {
        const resultData = response.data.data;
        dispatch({type: EDIT_ORDER_SUCCESS, payload: resultData});

        if (typeof afterSuccess === 'function')
          afterSuccess(resultData.id);
      })
      .catch(error => dispatch({ type: EDIT_ORDER_FAILURE, payload: error.response }) );
  }
}

export function updateOrder(id, data, afterSuccess) {
  return (dispatch) => {
    dispatch({ type: UPDATE_ORDER_REQUEST });
    return axios.put(`api/admin/orders/${id}`, data)
      .then(response => {
        const resultData = response.data.data;
        dispatch({ type: UPDATE_ORDER_SUCCESS, payload: resultData });

        if (typeof afterSuccess === 'function')
          afterSuccess(resultData.id);
      })
      .catch(error => dispatch({ type: UPDATE_ORDER_FAILURE, payload: error.response }) );
  }
}

export function deleteOrder(id, afterSuccess) {
  return (dispatch) => {
    dispatch({ type: DELETE_ORDER_REQUEST });
    return axios.delete(`api/admin/orders/${id}`)
      .then(response => {
        dispatch({ type: DELETE_ORDER_SUCCESS, payload: id });

        if (typeof afterSuccess === 'function')
          afterSuccess();
      })
      .catch(error => dispatch({ type: DELETE_ORDER_FAILURE, payload: error.response }) );
  }
}
