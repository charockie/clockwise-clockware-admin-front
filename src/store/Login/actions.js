import axios from 'axios';

export const CHANGE_FORM = 'CHANGE_FORM';

export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const NOT_AUTHENTICATED = 'NOT_AUTHENTICATED';

export function notAuthorised() {
  return (dispatch) => dispatch({ type: NOT_AUTHENTICATED })
}

export const changeForm = (name, value) => ({
  type: CHANGE_FORM,
  payload: {name: name, value: value},
})

export function authenticate({ email, password }) {
  return (dispatch) => {
    dispatch({ type: LOGIN_REQUEST });
    return axios.post('admin/login', {email, password})
      .then(response => {
        const resultData = response.data;
        localStorage.setItem("api_key", resultData.api_key);

        dispatch({ type: LOGIN_SUCCESS, payload: resultData.user });
      })
      .catch(error => dispatch({ type: LOGIN_FAILURE, payload: error.response }) );
  }
}

export function logout() {
  return (dispatch) => {
    localStorage.removeItem("api_key");

    dispatch({type: LOGOUT_SUCCESS});
  }
}
