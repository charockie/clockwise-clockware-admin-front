import { LOGIN_SUCCESS, LOGOUT_SUCCESS, NOT_AUTHENTICATED, CHANGE_FORM } from './actions';

const defaultState = {
  form: {
    email: '',
    password: '',
  },
  isAuthenticated: localStorage.hasOwnProperty("api_key"),
  user: {},
};

export const loginReducer = (state = defaultState, action) => {
  switch (action.type) {
    case CHANGE_FORM:
      const {name, value} = action.payload;
      return { ...state,
        form: {...state.form, [name]: value}
      };
    case LOGIN_SUCCESS:
      return {
        ...defaultState,

        isAuthenticated: true,
        user: action.payload
      };
    case LOGOUT_SUCCESS:
      return { ...defaultState,
        isAuthenticated: false,
      };
    case NOT_AUTHENTICATED:
      return { ...defaultState,
        isAuthenticated: false,
      };
    default:
      return state;
  }
}
