import axios from 'axios';

export const GET_CUSTOMERS_REQUEST = 'GET_CUSTOMERS_REQUEST';
export const GET_CUSTOMERS_SUCCESS = 'GET_CUSTOMERS_SUCCESS';
export const GET_CUSTOMERS_FAILURE = 'GET_CUSTOMERS_FAILURE';

export const GET_CUSTOMER_REQUEST = 'GET_CUSTOMER_REQUEST';
export const GET_CUSTOMER_SUCCESS = 'GET_CUSTOMER_SUCCESS';
export const GET_CUSTOMER_FAILURE = 'GET_CUSTOMER_FAILURE';

export const EDIT_CUSTOMER_REQUEST = 'EDIT_CUSTOMER_REQUEST';
export const EDIT_CUSTOMER_SUCCESS = 'EDIT_CUSTOMER_SUCCESS';
export const EDIT_CUSTOMER_FAILURE = 'EDIT_CUSTOMER_FAILURE';

export const UPDATE_CUSTOMER_REQUEST = 'UPDATE_CUSTOMER_REQUEST';
export const UPDATE_CUSTOMER_SUCCESS = 'UPDATE_CUSTOMER_SUCCESS';
export const UPDATE_CUSTOMER_FAILURE = 'UPDATE_CUSTOMER_FAILURE';

export const DELETE_CUSTOMER_REQUEST = 'DELETE_CUSTOMER_REQUEST';
export const DELETE_CUSTOMER_SUCCESS = 'DELETE_CUSTOMER_SUCCESS';
export const DELETE_CUSTOMER_FAILURE = 'DELETE_CUSTOMER_FAILURE';

export const CHANGE_PAGE = 'CHANGE_PAGE';

export const CHANGE_FORM = 'CHANGE_FORM';
export const RESET_FORM = 'RESET_FORM';

export const changeForm = (name, value) => ({
  type: CHANGE_FORM,
  payload: {name: name, value: value},
})

export function resetForm(resetId = false) {
  return (dispatch) => {
    dispatch({ type: RESET_FORM, payload: resetId });
    dispatch({ type: "RESET_VALIDATION_ERRORS" });
  };
}

export function changePage(newPage) {
  return (dispatch) => {
    this.fetchCustomersList(newPage);

    dispatch({
      type: CHANGE_PAGE,
      payload: newPage
    });
  }
}

export function fetchCustomersList(current_page = 1, per_page = 10) {
  return (dispatch) => {
    dispatch({ type: GET_CUSTOMERS_REQUEST });
    return axios.get(`/api/admin/customers?page=${current_page}&per_page=${per_page}`)
      .then(response => dispatch({ type: GET_CUSTOMERS_SUCCESS, payload: response.data }) )
      .catch(error => dispatch({ type: GET_CUSTOMERS_FAILURE, payload: error, error: true }) );
  }
}

export function fetchCustomer(id) {
  return (dispatch) => {
    dispatch({ type: GET_CUSTOMER_REQUEST });
    return axios.get(`api/admin/customers/${id}`)
      .then(response => dispatch({ type: GET_CUSTOMER_SUCCESS, payload: response.data.data }) )
      .catch(error => dispatch({ type: GET_CUSTOMER_FAILURE, payload: error, error: true }) );
  }
}

export function editCustomer(id, afterSuccess) {
  return (dispatch) => {
    dispatch({ type: EDIT_CUSTOMER_REQUEST });
    return axios.get(`api/admin/customers/${id}`)
      .then(response => {
        const resultData = response.data.data;
        dispatch({type: EDIT_CUSTOMER_SUCCESS, payload: resultData});

        if (typeof afterSuccess === 'function')
          afterSuccess(resultData.id);
      })
      .catch(error => dispatch({ type: EDIT_CUSTOMER_FAILURE, payload: error.response }) );
  }
}

export function updateCustomer(id, data, afterSuccess) {
  return (dispatch) => {
    dispatch({ type: UPDATE_CUSTOMER_REQUEST });
    return axios.put(`api/admin/customers/${id}`, data)
      .then(response => {
        const resultData = response.data.data;
        dispatch({ type: UPDATE_CUSTOMER_SUCCESS, payload: resultData });

        if (typeof afterSuccess === 'function')
          afterSuccess(resultData.id);
      })
      .catch(error => dispatch({ type: UPDATE_CUSTOMER_FAILURE, payload: error.response }) );
  }
}

export function deleteCustomer(id, afterSuccess) {
  return (dispatch) => {
    dispatch({ type: DELETE_CUSTOMER_REQUEST });
    return axios.delete(`api/admin/customers/${id}`)
      .then(response => {
        dispatch({ type: DELETE_CUSTOMER_SUCCESS, payload: id });

        if (typeof afterSuccess === 'function')
          afterSuccess();
      })
      .catch(error => dispatch({ type: DELETE_CUSTOMER_FAILURE, payload: error.response }) );
  }
}
